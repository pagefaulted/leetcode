package com.leetcode.bintree

import java.util.Stack

data class TreeNode(val value: Int) {
    var left: TreeNode? = null
    var right: TreeNode? = null
}

/**
 * Tailrec pre-order traversal.
 *        1
 *       / \
 *      2   3
 */
class PreOrderTraversal {
    fun preOrderTraversal(root: TreeNode?): List<Int> =
        when (root) {
            null -> emptyList()
            else -> {
                val stack: Stack<TreeNode> = Stack()
                stack.push(root)
                val result = mutableListOf<Int>()
                doTailrecPreOrderTraversal(stack, result)
                result
            }
        }

    private tailrec fun doTailrecPreOrderTraversal(stack: Stack<TreeNode>, result: MutableList<Int>): Unit =
        when {
            stack.isEmpty() -> {}
            else -> {
                val node = stack.pop()
                result.add(node.value)

                node.right?.let { stack.push(it) }
                node.left?.let { stack.push(it) }
                doTailrecPreOrderTraversal(stack, result)
            }
        }
}
