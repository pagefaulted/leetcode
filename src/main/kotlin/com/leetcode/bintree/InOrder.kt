package com.leetcode.bintree

import java.util.Stack

/**
 * In-order traversal.
 *      2
 *     / \
 *    1   3
 */

/**
 * Stack based loop in-order traversal.
 */
class InOrderTraversalLoop {
    fun inOrderTraversal(root: TreeNode?): List<Int> {
        if (root == null) {
            return emptyList()
        } else {
            val nodesToTraverse = pushAllLeft(root)
            val result = mutableListOf<Int>()

            while(nodesToTraverse.isNotEmpty()) {
                val leaf = nodesToTraverse.removeAt(nodesToTraverse.lastIndex)
                result.add(leaf.value)

                if (leaf.right != null) {
                    nodesToTraverse.addAll(pushAllLeft(leaf.right!!))
                }
            }
            return result
        }
    }

    private fun pushAllLeft(node: TreeNode): MutableList<TreeNode> {
        var n: TreeNode? = node
        val res = mutableListOf<TreeNode>()
        while (n != null) {
            res.add(n)
            n = n.left
        }
        return res
    }
}

/**
 * Tailrec in-order traversal.
 */
class InOrderTraversalTailrec {
    fun inOrderTraversal(root: TreeNode?): List<Int> =
        when (root) {
            null -> emptyList()
            else -> {
                val stack: Stack<TreeNode> = Stack()
                stack.addElement(root)
                val result = mutableListOf<Int>()
                doTailrecInOrderTraversal(stack, result, false)
                result
            }
        }

    private tailrec fun doTailrecInOrderTraversal(stack: Stack<TreeNode>, result: MutableList<Int>, backEdge: Boolean): Unit {
        if (stack.isEmpty()) {
            return
        } else {
            if (!backEdge) {
                if (stack.peek().left != null) {
                    // keep descending left
                    stack.addElement(stack.peek().left)
                    doTailrecInOrderTraversal(stack, result, false)
                } else {
                    // mo more left nodes, pop top and process it
                    val leaf = stack.pop()
                    result.add(leaf.value)

                    if (leaf.right != null) {
                        // still right sub-tree to process
                        stack.addElement(leaf.right)
                        doTailrecInOrderTraversal(stack, result, false)
                    } else {
                        // back track
                        doTailrecInOrderTraversal(stack, result, true)
                    }
                }
            } else {
                // back edge, process top of stack
                val leaf = stack.pop()
                result.add(leaf.value)
                if (leaf.right != null) {
                    // still right sub-tree to process
                    stack.addElement(leaf.right)
                    doTailrecInOrderTraversal(stack, result, false)
                } else {
                    // back track
                    doTailrecInOrderTraversal(stack, result, true)
                }
            }
        }
    }
}

fun main() {
    val one = TreeNode(1)
    val two = TreeNode(2)
    val three = TreeNode(3)

    two.left = three
    three.left = one

    println(InOrderTraversalTailrec().inOrderTraversal(two))
    println(InOrderTraversalLoop().inOrderTraversal(two))
}