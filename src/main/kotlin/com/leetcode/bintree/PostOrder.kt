package com.leetcode.bintree

/**
 * Post-order traversal.
 *    3
 *   / \
 *  1   2
 */
class PostOrder {

    fun postOrderTraversal(root: TreeNode?): List<Int> {
        return if (root == null) {
            emptyList()
        } else {
            val result = mutableListOf<Int>()
            val nodes = mutableListOf<TreeNode>()
            pushAll(root, nodes)

            while (nodes.isNotEmpty()) {
                val node = nodes.removeAt(0)
                result.add(node.value)
            }

            result
        }
    }

    private fun pushAll(root: TreeNode?, accum: MutableList<TreeNode>) {
        when (root) {
            null -> return
            else -> {
                pushAll(root.left, accum)
                pushAll(root.right, accum)
                accum.add(root)
            }
        }
    }
}

fun main() {
    val one = TreeNode(3)
    val two = TreeNode(1)
    val three = TreeNode(2)

    one.left = two
    one.right = three

    println(PostOrder().postOrderTraversal(one))
}