package com.leetcode.rotation

/**
 * Simple solution, O(n).
 */
class Solution {

    companion object {
        val num2rotated = mapOf(
            0 to 0,
            1 to 1,
            2 to 5,
            5 to 2,
            6 to 9,
            8 to 8,
            9 to 6
        )
    }

    private fun getRotated(n: Int): String? =
        n.toDigits().fold("") { acc, i ->
            acc + (num2rotated[i]?.let { it } ?: return null)
        }

    fun rotateDigits(n: Int): Int =
        1.rangeTo(n)
            .filter {
            val rotated = getRotated(it)
            rotated != null && rotated != it.toString()
        }.size
}

fun Int.toDigits(): List<Int> = this@toDigits.toString().asSequence().map(Character::getNumericValue).toList()
